# werbeo-app

## Play Store

  * https://play.google.com/store/apps/details?id=org.infinitenature.app.flora.st
  * https://play.google.com/store/apps/details?id=com.novimapp.werbeo.app
  * https://play.google.com/store/apps/details?id=com.novimapp.werbeo.app.heuschrecken
  
```
Before building please copy in project root the wished .env-xxxxx to .env
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
# then deploy dist to webserver
```
### Cordova
```
npm run cordova-build-ios # Build IOS
#then open project in XCode

npm run cordova-build-android # Build Android Production
npm run cordova-build-only-www-android # Build only files to src-cordova
cd src-cordova
cordova build android # Build Android debug apk without signing
```
### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
