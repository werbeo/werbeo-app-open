module.exports = {
  productionSourceMap: false,
  configureWebpack: {
    devtool: 'source-map',
  },
  publicPath: process.env.CORDOVA_PLATFORM && (process.env.CORDOVA_PLATFORM !== 'browser') ? ''
    : process.env.NODE_ENV === 'production'
    ? process.env.VUE_APP_PUBLIC_PATH
    : '/',
  pluginOptions: {
    cordovaPath: 'src-cordova'
  },
}
