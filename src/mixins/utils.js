import { CONFIG } from "../helpers/config";
// import coordinatesHelper from "./coordinatesHelper";

export default {
  methods: {
    singleOcc(sampleId) {
      let key = Object.keys(this.$storage.samples[sampleId].occurrences)[0];
      return this.$storage.samples[sampleId].occurrences[key];
    },
    initSamples(status, samples) {
      for (const s of samples) {
        let sample = {};
        sample.status = status;
        if (s.createdBy == this.$keycloak.userName) {
          sample.status = "Meine Funde";
        }
        sample.createdBy = s.createdBy;
        sample.id = s.id;
        sample.wktEpsg = s.locality.position.wktEpsg;
        sample.xCentroid = this.transformXCentroid(
          s.locality.position.posCenterLongitude,
          s.locality.position.posCenterLatitude,
          s.locality.position.wktEpsg
        );
        sample.yCentroid = this.transformYCentroid(
          s.locality.position.posCenterLongitude,
          s.locality.position.posCenterLatitude,
          s.locality.position.wktEpsg
        );
        if (s.locality.position.type == "POINT") {
          sample.type = "singleocc";
        } else {
          sample.type = "plot";
          sample.polyline = [];
          let coords = s.locality.position.wkt
            .split("((")[1]
            .split("))")[0]
            .split(",");
          for (let coord of coords) {
            coord = coord.trim();
            sample.polyline.push([coord.split(" ")[1], coord.split(" ")[0]]);
          }
        }
        sample.date = s.date.from + "T12:00";
        sample.blur = s.locality.blur;
        for (const attribute of this.$storage.attributes.sample[0].attributes) {
          if (attribute.attrName != "date" && attribute.attrName != "blur") {
            const props = attribute.attrName.split(".");
            if (props.length == 2)
              sample[attribute.attrName] = s[props[0]][props[1]];
            else sample[attribute.attrName] = s[attribute.attrName];
          }
        }
        sample.fotos = {};
        sample.occurrences = {};
        this.$set(this.$storage.samples, sample.id, sample);
        for (const occ of s.occurrences) {
          let occurrence = {};
          occurrence.id = occ.id;
          occurrence.speciesId = occ.taxon.id;
          occurrence.speciesName = occ.taxon.name;
          for (const attribute of this.$storage.attributes.occurrence[0]
            .attributes) {
            occurrence[attribute.attrName] = occ[attribute.attrName];
          }
          this.$set(
            this.$storage.samples[sample.id].occurrences,
            occurrence.id,
            occurrence
          );
        }
      }
    },
    checkForm(objType, obj) {
      let regInt = /^[1-9]\d*$/;
      // check form of singleocc
      if (objType == "occurrence") {
        if (!obj.speciesName) return "Bitte Taxon auswählen";
        else if (!obj.numericAmount && obj.numericAmountAccuracy)
          obj.numericAmountAccuracy = null;
      } else {
        if (obj.type == "singleocc" && !obj.xCentroid)
          return "Bitte auf der Karte Fundpunkt setzen";
      }
      for (const group of Object.values(this.$storage.attributes[objType])) {
        for (const attribute of Object.values(group.attributes)) {
          // set empty attributes to null
          if (obj[attribute.attrName] == "") obj[attribute.attrName] = null;

          if (attribute.mandatory && !obj[attribute.attrName])
            return "Bitte " + attribute.text + " eingeben!";
          if (obj[attribute.attrName]) {
            if (
              attribute.type == "int" &&
              !regInt.test(obj[attribute.attrName])
            )
              return (
                "Bitte " + attribute.text + " als positive Ganzzahl eingeben"
              );
            if (
              Number(obj[attribute.attrName]) < attribute.min ||
              Number(obj[attribute.attrName]) > attribute.max
            )
              return (
                attribute.text +
                " muss zwischen " +
                attribute.min +
                " und " +
                attribute.max +
                " liegen"
              );
            if (
              attribute.type == "text" &&
              obj[attribute.attrName].length > attribute.maxlength
            )
              return (
                attribute.text +
                " darf nicht mehr als " +
                attribute.maxlength +
                " Zeichen enthalten"
              );
          }
        }
      }
      return "";
    },
  },
};
