import { CONFIG } from '../helpers/config.js'
import localForage from 'localforage'
export default {
  methods: {
    createSample: function (type) {
      let sample = {}
      sample.type = type
      sample.id = CONFIG.UUID()
      sample.status = "In Bearbeitung"
      for (const group of Object.values(this.$storage.attributes.sample)) {
        for (const attribute of Object.values(group.attributes)) {
          sample[attribute.attrName] = null
        }
      }
      sample.date = new Date().toJSON()
      sample.occurrences = {}
      sample.fotos = {}
      sample.polyline = []
      sample.wktEpsg = "";
      this.$set(this.$storage.samples, sample.id, sample)
      return sample
    },
    deleteSample: function (sampleId) {
      const promise = new Promise((fulfill) => {
        CONFIG.dialog("Erhebung vom Gerät löschen?", null ,true)
          .then((result) => {
            if (result.value) {
              for (const key of Object.keys(this.$storage.samples[sampleId].fotos)) {
                localForage.removeItem('img' + key)
              }
              for (const occ of Object.values(this.$storage.samples[sampleId].occurrences)) {
                for (const key of Object.keys(occ.fotos)) {
                  localForage.removeItem('img' + key)
                }
              }
              this.$delete(this.$storage.samples, sampleId)
              fulfill()
            }
          })
      })
      return promise
    },
    deleteSampleNoMessage: function (sampleId) {
      const promise = new Promise((fulfill) => {
        for (const key of Object.keys(this.$storage.samples[sampleId].fotos)) {
          localForage.removeItem('img' + key)
        }
        for (const occ of Object.values(this.$storage.samples[sampleId].occurrences)) {
          for (const key of Object.keys(occ.fotos)) {
            localForage.removeItem('img' + key)
          }
        }
        this.$delete(this.$storage.samples, sampleId)
        fulfill()


      })
      return promise
    },
    deleteSamples(status) {
      for (const sample of Object.values(this.$storage.samples)) {
        if (sample.status == status) {
          for (const key of Object.keys(this.$storage.samples[sample.id].fotos)) {
            localForage.removeItem('img' + key)
          }
          this.$delete(this.$storage.samples, sample.id)
        }
      }
    },
    createOcc: function (sampleId) {
      let occurrence = {}
      occurrence.id = CONFIG.UUID()
      occurrence.fotos = {}
      for (const group of Object.values(this.$storage.attributes.occurrence)) {
        for (const attribute of Object.values(group.attributes)) {
          occurrence[attribute.attrName] = null
        }
      }
      occurrence.speciesId = null
      occurrence.speciesName = ""
      this.$set(this.$storage.samples[sampleId].occurrences, occurrence.id, occurrence)
      return occurrence
    },
    deleteOccurrence(sample, occ) {
      CONFIG.dialog("Erhebung vom Gerät löschen", null, true)
        .then((result) => {
          if (result.value) {
            for (const key of Object.keys(this.$storage.samples[sample].occurrences[occ].fotos)) {
              localForage.removeItem('img' + key)
            }
            this.$delete(this.$storage.samples[sample].occurrences, occ)
            this.$router.push({ name: 'sample', params: { id: sample } })
          }
        })
    },
    deleteOccurrenceNoMessage(sample, occ) {
      for (const key of Object.keys(this.$storage.samples[sample].occurrences[occ].fotos)) {
        localForage.removeItem('img' + key)
      }
      this.$delete(this.$storage.samples[sample].occurrences, occ)
      this.$router.push({ name: 'sample', params: { id: sample } })
    },
  }
}
