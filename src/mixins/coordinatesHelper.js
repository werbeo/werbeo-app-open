import proj4 from "proj4";
export default {
  methods: {
    transformXCentroid(x, y, wktEpsg) {
      let transformObject = {
        wktEpsg: wktEpsg,
        xCentroid: x,
        yCentroid: y,
      };
      let transformXArray = this.transformProjection(transformObject);
      return transformXArray[0];
    },
    transformYCentroid(x, y, wktEpsg) {
      let transformObject = {
        wktEpsg: wktEpsg,
        xCentroid: x,
        yCentroid: y,
      };
      let transformYArray = this.transformProjection(transformObject);
      return transformYArray[1];
    },

    transformProjection(sample) {
      proj4.defs([
        [
          "EPSG:31468",
          "+proj=tmerc +lat_0=0 +lon_0=12 +k=1 +x_0=4500000 +y_0=0 +ellps=bessel +datum=potsdam +units=m +no_defs ",
        ],
        [
          "EPSG:31469",
          "+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=5500000 +y_0=0 +ellps=bessel +datum=potsdam +units=m +no_defs",
        ],
        [
          "EPSG:31467",
          "+proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=3500000 +y_0=0 +ellps=bessel +datum=potsdam +units=m +no_defs",
        ],
        [
          "EPSG:4314",
          "+proj=longlat +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +no_defs",
        ],
      ]);
      let wktArray = ["EPSG:31467", "EPSG:31468", "EPSG:31469", "EPSG:4314"];
      let x = Number(sample.xCentroid);
      let y = Number(sample.yCentroid);
      let baseCoordinatesArray = [x, y];
      let result = [];
      if (typeof sample.wktEpsg != "undefined") {
        let wktString = "EPSG:" + String(sample.wktEpsg);
        wktArray.includes(wktString)
          ? (result = proj4(wktString, "WGS84", baseCoordinatesArray))
          : (result = baseCoordinatesArray);
      } else {
        console.log(
          "Wkt undefined returned original Array: ",
          baseCoordinatesArray
        );
        return baseCoordinatesArray;
      }
      // result is [x,y] in wgs84 or the default value
      return result;

    },
  },
};
