import localforage from 'localforage';

var lf = localforage.createInstance({
  name: 'leaflet_offline',
  version: 1.0,
  size: 40980736,
  storeName: 'tiles',
  description: 'the tiles',
});

export default lf;
