import Vue from "vue";
import axios from "axios";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import { CONFIG } from "../helpers/config.js";

NProgress.configure({ showSpinner: false });

axios.interceptors.request.use(
  (config) => {
    NProgress.start();
    if (Vue.prototype.$keycloak.authenticated)
      config.headers[
        "Authorization"
      ] = `Bearer ${Vue.prototype.$keycloak.token}`;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (response) => successHandler(response),
  (error) => errorHandler(error)
);

const successHandler = (response) => {
  NProgress.done();
  return response;
};
const checkHref = (stringToMatch) => {
  let actualHref = window.location.href;
  return actualHref.includes(stringToMatch);
};

const errorHandler = (error) => {
  NProgress.done();
  if (!error.response) {
    // console.error(retries);
    CONFIG.dialog("Keine Internetverbindung", "Bitte später versuchen", false);
  } else {
    const { status } = error.response;
    let errortext = error.response.data.message;
    if (error.response.data.errors) {
      errortext =
        error.response.data.errors.email ||
        error.response.data.errors.password ||
        error.response.data.errors.name;
    }
    switch (status) {
      case 400:
        checkHref("nearby")
          ? CONFIG.dialog(
              "Kartenausschnitt zu groß!",
              "Leider können wir Ihnen die vielen Funde nicht anzeigen. Bitte Zoomen sie weiter in die Karte."
            )
          : CONFIG.dialog(
              "Keine Taxa angegeben",
              "Bitte prüfen sie Ihre Angaben auf fehlerhafte Taxa oder leere Kartierlisten."
            );
        break;
      case 401:
        CONFIG.dialog(
          "Nicht angemeldet oder Sitzung abgelaufen!",
          "Bitte anmelden!",
          false
        ).then(() => this.$keycloak.login());
        break;
      case 500:
        CONFIG.dialog(
          "Fehler in Polygon",
          "Bitte überprüfen Sie ihre Kartierlisten auf mögliche Linienüberschneidungen in den Polygonen.",
          false
        );
        break;
      default:
        CONFIG.dialog(
          status ? "Fehler" + status : "Fehler",
          decodeURIComponent(
            encodeURIComponent(errortext ? errortext : error.response.data)
          ),
          false
        );
        break;
    }
    return Promise.reject(error);
  }
};
