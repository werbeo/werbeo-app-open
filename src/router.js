import Vue from "vue";
import Router from "vue-router";
import NearBy from "./views/Nearby.vue";
import NearbyTable from "./views/NearbyTable.vue";
import Sample from "./views/Sample.vue";
import Occurrence from "./views/Occurrence.vue";
import Samples from "./views/Samples.vue";
import RadioList from "./views/RadioList.vue";
import Map from "./views/Map.vue";
import MapOcc from "./views/MapOcc.vue";
import Menu from "./views/Menu.vue";
import SpeciesSearch from "./views/Speciessearch.vue";
import SingleOcc from "./views/Singleocc.vue";
import TermsCond from "./views/TermsCond.vue";
import About from "./views/About.vue";
Vue.use(Router);

export default new Router({
  mode: "hash",
  base: process.env.BASE_URL,

  routes: [
    {
      path: "/map/:id",
      name: "map",
      component: Map,
      props: true,
    },
    {
      path: "/mapocc/:sampleId/:occId",
      name: "mapocc",
      component: MapOcc,
      props: true,
    },
    {
      path: "/radiolist/:id/:attribute/:occ",
      name: "radiolist",
      component: RadioList,
      props: true,
    },
    {
      path: "/",
      name: "startmenu",
      component: Menu,
    },
    {
      path: "/samples/:status",
      name: "samples",
      component: Samples,
      props: true,
    },
    {
      path: "/sample/:id",
      name: "sample",
      component: Sample,
      props: true,
    },
    {
      path: "/singleocc/:sample/:occ",
      name: "singleocc",
      component: SingleOcc,
      props: true,
    },
    {
      path: "/menu",
      name: "menu",
      component: Menu,
    },
    {
      path: "/termscond",
      name: "termscond",
      component: TermsCond,
    },
    {
      path: "/about",
      name: "about",
      component: About,
    },
    {
      path: "/occurrence/:sample/:occ",
      name: "occurrence",
      component: Occurrence,
      props: true,
    },
    {
      path: "/speciessearch/:sample/:occ",
      name: "speciessearch",
      component: SpeciesSearch,
      props: true,
    },
    {
      path: "/nearby",
      name: "nearby",
      component: NearBy,
    },
    {
      path: "/nearbytable",
      name: "nearbytable",
      component: NearbyTable,
    },
  ],
});
