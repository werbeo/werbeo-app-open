import Device from './device';

const Image = {
  deleteInternalStorage(name, callback) {
    function errorHandler(err) {
      callback(err);
    }
    window.resolveLocalFileSystemURL(cordova.file.dataDirectory, (fileSystem) => {
      const relativePath = name.replace(fileSystem.nativeURL, '');
      fileSystem.getFile(relativePath, { create: false }, (fileEntry) => {
        if (!fileEntry) {
          callback();
          return;
        }

        fileEntry.remove(() => {
          callback();
        }, errorHandler);
      }, errorHandler);
    }, errorHandler);
  },
  getImage(callback, options = {}) {
    const cameraOptions = {
      sourceType: window.Camera.PictureSourceType.CAMERA,
      quality: 100,
      targetWidth: 2000,
      targetHeight: 2000,
      destinationType: window.Camera.DestinationType.FILE_URI,
      correctOrientation: true,
      allowEdit: false,
      encodingType: window.Camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
    }
    Object.assign(cameraOptions, options)
    console.log(cameraOptions)
    function onError(message) {
      const error = Error('Error capturing photo: ' + message);
      callback(error);
    }
    function fail(error) {
      callback(error);
    }
    function onSuccess(fileURI) {
      let URI = fileURI;
      // for some reason when selecting from Android gallery
      // the prefix is sometimes missing
      if (Device.isAndroid() &&
        cameraOptions.sourceType === window.Camera.PictureSourceType.PHOTOLIBRARY) {
        if (!(/file:\/\//).test(URI)) {
          URI = `file://${URI}`;
        }
      }
      window.resolveLocalFileSystemURL(URI, callback, fail);
    }
    navigator.camera.getPicture(onSuccess, onError, cameraOptions);
  },
}
export { Image as default };
