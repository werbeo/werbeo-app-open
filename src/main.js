import Vue from 'vue'
import VueKeycloakJs from './plugins/vue-keycloak-js'
import App from './App.vue'
import router from './router'
import localForage from 'localforage'
import 'localforage-getitems'
import Datetime from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.min.css'
import './plugins/axios'
import "typeface-roboto"
import 'material-icons/iconfont/material-icons.css'
import '@fortawesome/fontawesome-free/css/all.min.css'
import Dexie from 'dexie'
import axios from 'axios'
import { CONFIG } from './helpers/config.js'

if (window.cordova) {
  document.addEventListener("deviceready", function () {
    Vue.use(VueKeycloakJs, {
      init: {
        onLoad: 'check-sso',
        //        checkLoginIframe: false
      },
      config: {
        url: process.env.VUE_APP_URL,
        realm: process.env.VUE_APP_REALM,
        clientId: 'werbeo-app',
      }
    })
  }, false)
} else {
  Vue.use(VueKeycloakJs, {
    init: {
      onLoad: 'check-sso'
    },
    config: {
      url: process.env.VUE_APP_URL,
      realm: process.env.VUE_APP_REALM,
      clientId: 'werbeo-app',
    }
  })
}
Vue.use(Datetime)
Vue.config.productionTip = false

let config = {
  mapView: { center: [51, 11], zoom: 6 },
  overlays: [], baselayer: 'Reisekarte', read: false
}
let keys = ['samples', 'config', 'dict', 'meta', 'user', 'attributes']
// manual reset of database for reset change number behind dbVersion
let dbVersion = 40

if (localStorage.getItem('dbVersionWerBeo') != dbVersion) {
  window.indexedDB.deleteDatabase('WerBeo App ' + CONFIG.portal)
  window.indexedDB.deleteDatabase('WerBeo Dexie ' + CONFIG.portal)
}
localStorage.setItem('dbVersionWerBeo', dbVersion)
localForage.config({ name: 'WerBeo App ' + CONFIG.portal })
localForage.getItems(keys).then(values => {
  if (values.samples == null)
    values.samples = {}
  if (values.config == null) {
    values.config = config
  }
  else {
    values.config = Object.assign(config, values.config)
  }
  function readAttributes() {
    const promise = new Promise((resolve) => {
      if (values.attributes == null) {
        values.dict = {}
        axios.get(CONFIG.api + CONFIG.portal + '/translation/de').then(response => {
          values.dict = response.data.translations
          localForage.setItem('dict', values.dict)
          values.meta = { samples: {}, occurrences: {} }
          axios.get(CONFIG.api + CONFIG.portal + '/samples/meta/' + CONFIG.survey).then(response => {
            for (const fieldConfig of response.data.fieldConfigs) {
              values.meta.samples[fieldConfig.field] = fieldConfig
            }
            axios.get(CONFIG.api + CONFIG.portal + '/occurrences/meta/' + CONFIG.survey).then(response => {
              for (const fieldConfig of response.data.fieldConfigs) {
                values.meta.occurrences[fieldConfig.field] = fieldConfig
              }
              localForage.setItem('meta', values.meta)
              values.attributes = { sample: [{ groupName: "", attributes: [] }], occurrence: [{ groupName: "", attributes: [] }] }
              let fields
              if (CONFIG.portal == 6) {
                fields = [
                  { key: "DATE", type: "date" },
                  { key: "BLUR", type: "int", min: "1", max: "450000" },
                  { key: "SAMPLE_METHOD", type: "string" },
                ]
              }

              else if (CONFIG.portal == 2) {
                fields = [
                  { key: "DATE", type: "date" },
                  { key: "BLUR", type: "int", min: "1", max: "450000" },
                  { key: "LOCALITY", type: "text", maxlength: 150 },
                  { key: "LOCATION_COMMENT", type: "text", maxlength: 150 },
                ]
              }
              else if (CONFIG.portal == 5) {
                fields = [
                  { key: "DATE", type: "date" },
                  { key: "BLUR", type: "int", min: "1", max: "450000" },
                  { key: "LOCALITY", type: "text", maxlength: 150 },
                  { key: "LOCATION_COMMENT", type: "text", maxlength: 150 },
                ]
              }
              else if (CONFIG.portal == 11) {
                fields = [
                  { key: "DATE", type: "date" },
                  { key: "BLUR", type: "int", min: "1", max: "450000" },
                  { key: "LOCALITY", type: "text", maxlength: 150 },
                  { key: "LOCATION_COMMENT", type: "text", maxlength: 150 },
                ]
              }
              for (const field of fields) {
                let index = values.attributes.sample[0].attributes.push({
                  attrName: values.meta.samples[field.key].property,
                  type: field.type,
                  min: field.min ? field.min : null,
                  max: field.max ? field.max : null,
                  maxlength: field.maxlength ? field.maxlength : null,
                  text: values.dict[values.meta.samples[field.key].translationKey],
                  mandatory: values.meta.samples[field.key].mandantory,
                  values: []
                })
                for (const value of values.meta.samples[field.key].values) {
                  values.attributes.sample[0].attributes[index - 1].values.push({
                    value: value.value,
                    text: values.dict[value.translationKey]
                  })
                }
              }
              if (CONFIG.portal == 6) {
                fields = [
                  { key: "NUMERIC_AMOUNT", type: "int", min: "0", max: "450000" },
                  { key: "REPRODUCTION", type: "string" },
                  { key: "LIFE_STAGE", type: "string" },
                  { key: "MAKROPTER", type: "string" },
                  { key: "SEX", type: "string" },
                  { key: "DETERMINATION_COMMENT", type: "text", maxlength: 50 },
                ]
              }
              else if (CONFIG.portal == 2) {
                fields = [
                  { key: "OBSERVERS", type: "string" },
                  { key: "AREA", type: "string" },
                  { key: "SETTLEMENT_STATUS_FUKAREK", type: "string" },
                  { key: "BLOOMING_SPROUTS", type: "string" },
                  { key: "HABITAT", type: "string" },
                  { key: "VITALITY", type: "string" },
                  { key: "NUMERIC_AMOUNT", type: "int", min: "0", max: "450000" },
                  { key: "NUMERIC_AMOUNT_ACCURACY", type: "string" },
                ]
              }
              else if (CONFIG.portal == 5) {
                fields = [
                  { key: "OBSERVERS", type: "string" },
                  { key: "REMARK", type: "text", maxlength: 250 },
                  { key: "AREA", type: "string" },
                  { key: "HABITAT", type: "string" },
                  { key: "SETTLEMENT_STATUS_FUKAREK", type: "string" },
                  { key: "BLOOMING_SPROUTS", type: "string" },
                  { key: "VITALITY", type: "string" },
                  { key: "AMOUNT", type: "string" },
                  { key: "NUMERIC_AMOUNT", type: "int", min: "0", max: "450000" },
                  { key: "NUMERIC_AMOUNT_ACCURACY", type: "string" },
                ]
              }
              else if (CONFIG.portal == 11) {
                fields = [
                  { key: "OBSERVERS", type: "string" },
                  { key: "AREA", type: "string" },
                  { key: "SETTLEMENT_STATUS_FUKAREK", type: "string" },
                  { key: "BLOOMING_SPROUTS", type: "string" },
                  { key: "HABITAT", type: "string" },
                  { key: "VITALITY", type: "string" },
                  { key: "NUMERIC_AMOUNT", type: "int", min: "0", max: "450000" },
                  { key: "NUMERIC_AMOUNT_ACCURACY", type: "string" },
                ]
              }
              for (const field of fields) {
                let index = values.attributes.occurrence[0].attributes.push({
                  attrName: values.meta.occurrences[field.key].property,
                  type: field.type,
                  min: field.min ? field.min : null,
                  max: field.max ? field.max : null,
                  maxlength: field.maxlength ? field.maxlength : null,
                  text: values.dict[values.meta.occurrences[field.key].translationKey],
                  mandatory: values.meta.occurrences[field.key].mandantory,
                  values: []
                })
                for (const value of values.meta.occurrences[field.key].values) {
                  values.attributes.occurrence[0].attributes[index - 1].values.push({
                    value: value.value,
                    text: values.dict[value.translationKey]
                  })
                }
              }
              localForage.setItem('attributes', values.attributes)
              resolve("loaded")
            })
          })
        })
      }
      else
        resolve("reused")
    })
    return promise
  }
  readAttributes().then((result) => {
    console.log(result)
    const db = new Dexie("WerBeo Dexie " + CONFIG.portal)
    db.version(1).stores({
      taxa: "id, *genus, *species"
    })
    db.open()
      .then(() => {
        window.db = db
        Vue.prototype.$storage = new Vue({
          data() {
            return {
              samples: values.samples,
              user: values.user,
              config: values.config,
              attributes: values.attributes,
              meta: values.meta,
              dict: values.dict,
              taxaReady: false
            }
          },
          watch: {
            samples: {
              handler() {
                localForage.setItem('samples', this.samples)
              }, deep: true
            },
            config: {
              handler() {
                localForage.setItem('config', this.config)
              }, deep: true
            },
            user: {
              handler() {
                localForage.setItem('user', this.user)
              }, deep: true
            }
          }
        })
        new Vue({
          router,
          render: h => h(App)
        }).$mount('#app')
      })
  })
})
